# KN01: Virtualisierung

## A) Installieren Sie Virtual Machine Software

Ich habe "Virtual Box 7.0" als meine VM Software installiert. Hier kann man es installieren: [Link](https://www.virtualbox.org/wiki/Downloads)

## B) Erstellen sie ihre erste VM

Das Betrieb System habe ihc Ubuntu (22.04 LTS) ausgewählt. Ich habe die Server version ausgewählt, da es minimal ist und braucht keine Grafische Darstellung. Hier kann man es installieren: [Link](https://releases.ubuntu.com/jammy/)

## C) Ressourcenzuteilung

Bei erstellung einer neuen VM in Virtual Box (Unter "Tools" -> "New" anclicken) kann man die ISO datei der Betriebsystem auswählen und die Resourcen für das VM zugeben

![](./Images/Screenshot%202023-08-21%20094312.png)

### Hier gibt man die Resourcen des Vrtaul Maschine

![](./Images/Screenshot%202023-08-21%20091816.png)

### Am ende sollte das VM so aussehen:

![](./Images/Screenshot%202023-08-21%20091916.png)

### Wenn man auf der VM Page ist, kann man auf "Start" klicken, wenn es nicht automatisch startet

![](./Images/Screenshot%202023-08-21%20095355.png)

nach der installation von Ubuntu, kann ich das benutzte CPU und RAM ansehen mit `lscpu | grep "CPU(s)"` und `gree -g`

### Bild von VM CPU

![](./Images/Screenshot%202023-08-21%20093231.png)

### Bild von VM RAM

![](./Images/Screenshot%202023-08-21%20093749.png)

### Hier sind meine Resourcen auf meinem Laptop

![](./Images/Screenshot%202023-08-21%20093118.png)

Diese Beispiel waren mit Resourcen, weniger als mein Betriebsystem hat. Wenn ich aber versuchte das Vm zu verändern und mehr CPU Cores oder RAM hinzugeben, hat Virtual Box mir nicht erlaubt. Meiner Meinung macht das Sinn, da wie kann man mehr resourcen an einem VM geben, wenn das hauptsystem es selber nicht hat.

(wenn ich versuchte die eins auf neun zu setzen, hat es mir die Zahl nicht gelassen zu schreiben)

![](./Images/Screenshot%202023-08-21%20093454.png)

darum konnte ich keine Screenshots mit mehr CPU / RAM als mein Hauptbetriebsystem erstellen
