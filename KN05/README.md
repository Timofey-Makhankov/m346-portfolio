# KN05

## Diagramm erstellen

![Diagramm Bild](./Images/diagram.png)

## Inbound Rules

### DB Server (launch-wizard-7)

|          id         | IP | port |  protocol  | Type  |  source   |
|---------------------|----|------|------------|-------|-----------|
|sgr-0c54a683e30adc335|IPv4|  22  |    TCP     |  SSH  | 0.0.0.0/0 |
|sgr-063a587fe0779a116|IPv4| 3306 |    TCP     | MYSQL | 0.0.0.0/0 |

### Webserver (launch-wizard-6)

|          id         | IP | port |  protocol  | Type  |  source   |
|---------------------|----|------|------------|-------|-----------|
|sgr-042238bd4c2335a7a|IPv4|  22  |    TCP     |  SSH  | 0.0.0.0/0 |
|sgr-05e4ba541a652008e|IPv4|  80  |    TCP     | HTTP  | 0.0.0.0/0 |

## Outbound Rules

### DB Server (launch-wizard-7)

|          id         | IP | port |  protocol  | Type  |  source   |
|---------------------|----|------|------------|-------|-----------|
|sgr-07e656454e584a087|IPv4| ALL  |    ALL     |  ALL  | 0.0.0.0/0 |

### Webserver (launch-wizard-6)

|          id         | IP | port |  protocol  | Type  |  source   |
|---------------------|----|------|------------|-------|-----------|
|sgr-0178f1a73d05f7b37|IPv4| ALL  |    ALL     |  ALL  | 0.0.0.0/0 |

## Subnetz und private IP wählen

### Subnetze mit Namen

![subnetze mit Namen](./Images/Screenshot%202023-09-25%20082638.png)

### Ip Adressen

#### DB

ip: 172.31.67.130

#### Webserver

ip: 172.31.73.235

## Objekte und Instanzen erstellen

### Das sind die erstellten Security Gruppen

![](./Images/Screenshot%202023-09-25%20095729.png)

### Das ist die inbound regeln für Datenbank

![](./Images/Screenshot%202023-09-25%20095837.png)

### Das sind die Elastic IP, die zu instanzen zugegeben sind

![](./Images/Screenshot%202023-09-25%20095900.png)

### Das ist die index.html von apache mit der Elastic Webserver IP

![](./Images/Screenshot%202023-09-25%20100703.png)

### Das ist die info.php Datei mit der Elastic Webserver IP

![](./Images/Screenshot%202023-09-25%20100717.png)

### Das ist die db.php Datei mit der Elastic Webserver IP

![](./Images/Screenshot%202023-09-25%20100726.png)

### Das ist die Liste der Instanzen, wenn abgeschaltet

![](./Images/Screenshot%202023-09-25%20100004.png)

### Hier ist die Detail von Datenbank EC2

![](./Images/Screenshot%202023-09-25%20100136.png)

### Hier ist die Detail von Webserver EC2

![](./Images/Screenshot%202023-09-25%20100200.png)