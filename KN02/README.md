# KN02: Iaas - Virtuelle Server

## AWS Kurs

## A) Lab 4.1 - EC2

### Das ist die HTML-Webseite mit dem URL (54.162.197.157)

![](./Images/Screenshot%202023-08-28%20083411.png)

### Auf diesem Dashboard kann man alle EC2-Instancen sehen und die Details der Web-Server instance

![](./Images/Screenshot%202023-08-28%20083545.png)

### Die Security-Group: Liste der Inbound-Regeln der EC2 instance

![](./Images/Screenshot%202023-08-28%20083435.png)

## B) Lab 4.2 - S3

### Dashboard für alle Bucket instanzen

![](./Images/Screenshot%202023-08-28%20090346.png)

### Die HTML Webseite mit dem URL

![](./Images/Screenshot%202023-08-28%20090356.png)

### Liste der Dateien im Bucket

![](./Images/Screenshot%202023-08-28%20090414.png)

### Eigenschaften von "Static website hosting"

![](./Images/Screenshot%202023-08-28%20090433.png)
![](./Images/Screenshot%202023-08-28%20090450.png)

## B) Zugriff mit SSH-key

### verbindung mit SSH mit den ausgewählten schlüssel

![](./Images/Screenshot%202023-08-28%20093227.png)

### verbindung mit einem anderen Schlüssel

![](./Images/Screenshot%202023-08-28%20093318.png)

### Instanz-Detail mit den verwendbaren Schlüssel

![](./Images/Screenshot%202023-08-28%20093403.png)

## Installation von Web- und Datenbankserver

### Das ist der Link mit der index.html

![](./Images/Screenshot%202023-08-28%20095142.png)

### Das ist der Link mit der info.php

![](./Images/Screenshot%202023-08-28%20095152.png)

### Das ist der Link mit der db.php

![](./Images/Screenshot%202023-08-28%20095202.png)
