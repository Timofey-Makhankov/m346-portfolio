# KN06: Skalierung

## A) Installation App

### Reverse Proxy

Ein Reverse Proxy ist ein Server, dass zwischen dem Client und dem ziel Server sitzt. Es bearbeitet die Client requests und entscheidet, wohin dieses Request hingeschikt werden soll. meistens ist es ein Web Server und DB Server oder mehrere Webservern im Netzwerk. heutzutage kann man nginx mit einer config datei aufsetzen.

![](https://cf-assets.www.cloudflare.com/slt3lc6tev37/3msJRtqxDysQslvrKvEf8x/f7f54c9a2cad3e4586f58e8e0e305389/reverse_proxy_flow.png)

Quelle: [Cloudflare reverse-proxy](https://www.cloudflare.com/learning/cdn/glossary/reverse-proxy/)

### Swagger Webpage vom Server

![](./Images/Screenshot%202023-10-23%20092330.png)

### Endpoint "GetProducts" aufgeruft und resultat

![](./Images/Screenshot%202023-10-23%20092405.png)

### MongoDB Collection "shop.product"

![](./Images/Screenshot%202023-10-23%20092456.png)

## B) Vertikale Skalierung

Ich kann den Webserver sehr einfach den Speicher erweitern unter EC2 > Elastic Block Store > Volumes. den Speicher vom Webserver auswählen und eine grösse geben und erweitern.

### vorher

Ich habe bemerkt, dass man kann dass volumen erweitern, aber nicht reduzieren, darum habe ich nicht das gleiche bild für die gleiche instance. Es zeigt auch, dass es t2.micro benutzt

![andere instance](./Images/Screenshot%202023-10-23%20095156.png)

### nacher

#### Micro zu medium und 20GB Speicher platz

![](./Images/Screenshot%202023-10-23%20093556.png)

![](./Images/Screenshot%202023-10-23%20095401.png)

## C) Horizontale Skalierung

bei der Horizontale habe ich zwei instanzen erstellt mit der gleichen cloud-init datei. nachdem ich sie erstellt habe, habe ich sie in eine sogenannte 'Target Group' hinzugefügt und es erstellt. Es wird gebraucht, um ein load balancer zu definieren und erstellen

### Target Group

![](./Images/Screenshot%202023-10-30%20085059.png)

![](./Images/Screenshot%202023-10-23%20113132.png)

### Load Balancer

![](./Images/Screenshot%202023-10-23%20113103.png)

### Webseite durch Load Balancer

#### [URL](http://load-balancer-net-webserver-1217120976.us-east-1.elb.amazonaws.com/swagger/index.html)

![](./Images/Screenshot%202023-10-23%20113308.png)

![](./Images/Screenshot%202023-10-23%20113327.png)

### DNS konfigurieren

wenn meine applikation unter 'app.tbz-m346.ch' laufen würde, musste ich die DNS so konfigurieren, dass wenn es diesen URI sieht, soll es zum load-balancer url anzeigen. Im Load-Balancer Detail page wird ein DNS Record zugegeben, um es zu definieren.

## D) Auto Skalierung

Mit der Auto scaler kann ich extra instanzen aufstellen, wenn es gebraucht wird. Die Healthchecks werden auf die swagger doc geleitet.

### Auto Scaler

![](./Images/Screenshot%202023-10-23%20113150.png)

### Instanzen

![](./Images/Screenshot%202023-10-23%20113221.png)

## E Zusatz

Die Hard-coded Werte von der Datenbank verbindung soll für security und funktionalität nicht raufgeschrieben. Da vielleicht würde wir gerne eine andere MongoDB Server verbinden, müssten wir immer die Werte setzten. Das andere Problem ist, dass unsere login details auf "Plain-text" geschrieben, jeder kann sie sehen, wer die Datei zugriff hat. Man sollte mit system enviroments und "Secrets arbeiten"