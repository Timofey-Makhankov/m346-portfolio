# KN08: Faas und Backup

## A) Backup-Skript

### Instanzen mit Tags

![](./Images/Screenshot%202023-10-30%20100527.png)

### Nach 'Create backup' lambda Funktion

![](./Images/Screenshot%202023-10-30%20100624.png)

### Tags einer Snapshots

![](./Images/Screenshot%202023-10-30%20100651.png)

### Nach 'Clear Up' lambda Funktion

![](./Images/Screenshot%202023-10-30%20100901.png)

## B) Cron-job (Cloud Watch)

![](./Images/Screenshot%202023-10-30%20102840.png)

![](./Images/Screenshot%202023-10-30%20103144.png)

![](./Images/Screenshot%202023-10-30%20103159.png)