# KN03: Cloud-init und AWS

## A) Cloud-init Datei verstehen

```yaml
#cloud-config
users:
  - name: ubuntu # benutzername
    sudo: ALL=(ALL) NOPASSWD:ALL # sudo regeln für diesen benutzer
    groups: users, admin # zu welcher Gruppe der user zugehört
    home: /home/ubuntu # Das home directory für den user
    shell: /bin/bash # den default shell für den user
    ssh_authorized_keys: # Das SSH key für den user
      - ssh-rsa AAAA...qyBD aws-key       
ssh_pwauth: false # ssh password authentifizeiren deaktivieren
disable_root: false # root soll aktiviert werden
package_update: true # es soll alle packages aktualisieren
packages: # extra packages installieren
  - curl 
  - wget 

```

## B) SSH-Key und Cloud-init

### Details der instanze mit den benutzten key

![](./Images/Screenshot%202023-08-28%20110441.png)

### verbindenung mit den richtigen schlüssel

![](./Images/Screenshot%202023-08-28%20110206.png)

### verbindung mit den anderen schlüssel

![](./Images/Screenshot%202023-08-28%20110229.png)

### den ersten ausschnitt von cloud-init-log datei

![](./Images/Screenshot%202023-08-28%20110420.png)

## C) Template

```yaml
#cloud-config
users:
  - name: ubuntu 
    sudo: ALL=(ALL) NOPASSWD:ALL 
    groups: users, admin 
    home: /home/ubuntu 
    shell: /bin/bash 
    ssh_authorized_keys: 
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2mKZ+hNVKX3uHgX0jrME3jmwlwT+4kTdwW5sZRKLX6hq7f7GrMUOZcWMe3rGV3Fs94gy2XNx86XkTVyJfj0pueT+8aHWXVxmxq2wVOqNHHJJKktc9zWXGXLW6vFtsRtNWIAocEtsxCBFooKE3JGYGe2/20LQKqBqUk5a83Nq1vUqblaU/6dSHuvCxheqacJ50eQGreVZQfkfPIav73H5VZZCrqLLp9jjqYXBJztVC5zELxQMlWYkMtD62QVXax0R6U75kgzhN4dZbX7vxd2C3KBSNLyNVCjCgO6y3cktgA+VZc3tpi8aTx3hc7VwB5Y/xi3FQxJ8OtrHeKpKLlvVL aws-key # personal key
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC0WGP1EZykEtv5YGC9nMiPFW3U3DmZNzKFO5nEu6uozEHh4jLZzPNHSrfFTuQ2GnRDSt+XbOtTLdcj26+iPNiFoFha42aCIzYjt6V8Z+SQ9pzF4jPPzxwXfDdkEWylgoNnZ+4MG1lNFqa8aO7F62tX0Yj5khjC0Bs7Mb2cHLx1XZaxJV6qSaulDuBbLYe8QUZXkMc7wmob3PM0kflfolR3LE7LResIHWa4j4FL6r5cQmFlDU2BDPpKMFMGUfRSFiUtaWBNXFOWHQBC2+uKmuMPYP4vJC9sBgqMvPN/X2KyemqdMvdKXnCfrzadHuSSJYEzD64Cve5Zl9yVvY4AqyBD aws-key # teacher key
ssh_pwauth: false 
disable_root: false 
package_update: true 
packages: # extra packages installieren
  - curl 
  - wget 
  - apache2
  - php
  - libapache2-mod-php
  - mariadb-server
  - php-mysqli
  - adminer
write_files:
    - path: /var/www/html/db.php
      permissions: "0644"
      content: |
        <?php
          //database
          $servername = "172.31.35.217";
          $username = "admin";
          $password = "password";
          $dbname = "mysql";

          // Create connection
          $conn = new mysqli($servername, $username, $password, $dbname);
          // Check connectionsa<
          if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
          }

          $sql = "select Host, User from mysql.user;";
          $result = $conn->query($sql);
          while($row = $result->fetch_assoc()){
                  echo($row["Host"] . " / " . $row["User"] . "<br />");
          }
          //var_dump($result);
        ?>
runcmd:
  - sudo mysql -sfu root -e "GRANT ALL ON *.* TO 'admin'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;"
  - sudo sed -i 's/127.0.0.1/0.0.0.0/g' /etc/mysql/mariadb.conf.d/50-server.cnf
  - sudo a2enconf adminer
  - sudo systemctl restart mariadb.service
  - sudo systemctl restart apache2

```

## D) Auftrennung von Web- und Datenbankserver

### Die Verbindung zum DB durch DB Server

![](./Images/Screenshot%202023-09-04%20090929.png)

### Die Verbindung mit telnet zum DB Server

![](./Images/Screenshot%202023-09-04%20091600.png)

### Die index.html webseite vom Webserver

![](./Images/Screenshot%202023-09-04%20090658.png)

### Die info.php Page vom Webserver

![](./Images/Screenshot%202023-09-04%20092004.png)

### Die db.php Page vom Webserver

![](./Images/Screenshot%202023-09-04%20092023.png)

### Die Adminer verbindung vom webserver zu DB server

![](./Images/Screenshot%202023-09-04%20091652.png)
