# KN07 - Kostenberechnung

## A) Kostenrechnung erstellen

### Rehosting

#### AWS

Mit AWS habe ich zwei EC2 Instacen, die linux laufen. Ein Instace für den Web Server und der andere für Datenbank. Ein Loadbalancer musste zugegeben und Backup konnte auhc gegeben. 

![](./Images/Screenshot%202023-10-02%20085732.png)

#### Azure

ähnlich wie bei AWS, aber andere namen. Ich erstelle zwei Virtual machines mit Ubuntu. eins für Webserver und andere für Datenbank. Loadbalancer ist gratis, aber ich habe es erweitert, damit es muss nicht weiter konfiguriert. das Standart auswahl ist gut genug. Azure Backup konnte ich auch erstellen.

![](./Images/Screenshot%202023-10-02%20085720.png)

### Replatforming

#### Heroku

Ich habe bei Heroku zwei "Dyno" instances erstellt, das eine für den Webserver, das andere ist für den loadbalancer, da bei heroku gibt es keines und muss selber erstellt werden. Ich hbae auch ein Postgres Instace für Datenbank. Ich weiss nicht, ob es gibt ein backup solution

![](./Images/Screenshot%202023-10-02%20090519.png)

### Repurchasing

#### Zoho - CRM

Ich würde Enterprise auswählen, da es die meisten features und ist günstiger als ultimate. da habe ich wenig auswahl.

![](./Images/Screenshot%202023-10-02%20090758.png)

#### Salesforce

Be salesforce habe ich die Enterprice option ausgewählt, da den vergleich zu anderen Produkten, kommt es sehr nahe und habe features, die ich brauche. Ist baer schwierig zu herausfinden, was genau dieses Produkt gibt.

![](./Images/Screenshot%202023-10-02%20090935.png)


Ich würde die IaaS von Azure auswählen, da ich viel mehr kontrolle und auswahl habe. Bei Amazon war es schweiriger, die services zu definieren und bei Heroku habe ich zu wenig auswahl zu hardware. Bei SaaS habe ich die schwierigkeit, ob die solutions für den Betrieb gut sind.

## B) Interpretation der Resultate

### Preise (in USD)

| Service         | monatlich   | jährlich  |
|-----------------|-------------|-----------|
| AWS IaaS        | 183.17      | 2,198.04  |
| Azure IaaS      | 165.30      | 1’983.6   |
| Heroku PaaS     | 150         | 1’800     |
| Zoho SaaS       | 40 (42.34)  | (508.08)  |
| Salesforce cloud| 165 (174.65)| (2’095.8) |

**caviat: Zoho CRM ist Pro Benutzer, nicht system**

Das Billigste ist Zoho pro person, heroku als Produkt.

bei zoho ist es eben billig, wenn es wenig administratoren benutzen müssen, bei heroku sind die Modulen schon vordefiniert und können sie billiger anbieten, aber es hat weniger optionen für configuration. ansonsten sind die andere Services sehr nahe zu ihren Preisen. da muss man schauen, ob man möchte extra am anfang zahlen, um das solution zu konfigurieren oder vom Betrieb erstellen.