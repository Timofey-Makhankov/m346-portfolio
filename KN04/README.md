# KN04: Cloud-init / Storage

## A) Bild erstellen und auf S3 hosten

### Mein S3 Bucket mit allen objekten

![](./Images/Screenshot%202023-09-04%20093703.png)

### Das Bild mit dem persöhnliche URL

![](./Images/Screenshot%202023-09-04%20093751.png)

## B) Web-Server mit PHP-Seite hinzufügen

### Die Webseite mit einem Bild vom S3 Bucket

![](./Images/Screenshot%202023-09-04%20102012.png)

## C) Elastic Block Storage (EBS) hinzufügen

### Alle Speicher Volumen von dem Web server

![](./Images/Screenshot%202023-09-04%20104750.png)

## D) Speichereigenschaften erkennen

|                          | Typ  | Persitenz |
|           ---            | ---  |    ---    |
| EBS Root                 | hot  |    nein   |
| EBS Zusätzliches Volumen | warm |     ja    |
| S3                       | cold |     ja    |

Bei der Typ Art wird beschrieben, wie schnell der zugriff von Daten ist und wie wichtig es ist. Bei dem Root Drive ist sehr kritisch und wichtig, damit der Server schnell laufen kann. Bei einem S3 Bucket sind es meistens grosse Daten, die nicht oft gelesen werden müssen. Es gibt eie gute Darstellung dafür:

![Diagramm von Speicher Typen](./Images/Ctera-Cool-Medium-Hot-Graphic-051122.webp)

Quelle: https://www.ctera.com/company/blog/differences-hot-warm-cold-file-storage/

Bei der Persistanz wird gemeint, ob die gespeicherten Daten nach neustart bleiben oder gelöscht werden. Bei dem Root Drive werden alle Daten gelöscht. Bei erweiterten EBS oder S3 ist es nicht der fall
