terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_security_group" "web_security" {
  name        = "Web Server Security Group"
  description = "This is a description"

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "web_security"
  }
}

resource "aws_security_group_rule" "ssh_rule" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.web_security.id
}

resource "aws_security_group_rule" "http_rule" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.web_security.id
}

resource "aws_s3_bucket" "b1" {
  bucket = "my_bucket"
  tags = {
    Name        = "s3Bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_object" "provision_source_file" {
  bucket = aws_s3_bucket.b1.id
  key    = "profile"
  acl    = "public-read"
  source = "myfiles/yourfile.txt"
}

resource "aws_instance" "app_server" {
  ami             = "ami-053b0d53c279acc90"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.web_security.id]
  user_data       = "./file/location"
  tags = {
    Name = "Web Server"
  }
}