# KN09: Automation

## A) Automatisierung mit Command Line Interface (CLI)

### Starting EC2 instace/s

```bash
aws ec2 start-instances --instance-ids <INSTANCE-ID>
```

### Stopping EC2 instace/s

```bash
aws ec2 stop-instances --instance-ids <INSTANCE-ID>
```

### List Instaces

``` bash
aws ec2 describe-instances
```

> You may need to add `--region us-east-1` at the end, if region is not defined in config file

### Create Instace

```bash
aws ec2 run-instances --image-id <IMAGE-ID> --count 1 --instance-type t2.micro --key-name MyKeyPair --security-group-ids <SECURITY-GROUP-ID> --subnet-id <SUBNET-ID>
```

### Aufgabe

![](./Images/Screenshot%202023-10-30%20110929.png)

### Befehle

```bash
aws ec2 stop-instances --instance-ids i-049746d7cc35621cb
aws ec2 start-instances --instance-ids i-049746d7cc35621cb

aws ec2 run-instances --image-id ami-053b0d53c279acc90 --count 1 --instance-type t2.micro --key-name timofey-1 --security-group-ids sg-08489a2a10921a52b --subnet-id subnet-0bf692d054d1793b0 --user-data file://./cloud-init-db.yaml --tag-specifications 'ResourceType=instance, Tags=[{Key=Name, Value=cli-db}]'

aws ec2 run-instances --image-id ami-053b0d53c279acc90 --count 1 --instance-type t2.micro --key-name timofey-1 --security-group-ids sg-0631d88690b1ddc3e --subnet-id subnet-0bf692d054d1793b0 --user-data file://./cloud-init-web.yaml --tag-specifications 'ResourceType=instance, Tags=[{Key=Name, Value=cli-web}]'
```

ami-053b0d53c279acc90 = Ubuntu Server

![](./Images/Screenshot%202023-10-30%20112420.png)

### Telnet screenshot

![](./Images/Screenshot%202023-11-06%20092636.png)

### Befehle für KN04 mit AWS CLI

```bash
aws ec2 allocate-address --public-ipv4-pool ipv4pool-ec2-1234567890abcdef0

aws ec2 create-security-group --group-name MySecurityGroup --description "My security group" --vpc-id vpc-1a2b3c4d

aws ec2 create-subnet --vpc-id vpc-081ec835f3EXAMPLE --cidr-block 10.0.0.0/24 --tag-specifications ResourceType=subnet,Tags=[{Key=Name,Value=my-subnet}]

aws s3api create-bucket --bucket my-bucket --region us-east-1 --grant-full-controll

aws s3 cp /path/to/source s3://bucket-name/ --recursive

aws ec2 run-instances --image-id ami-053b0d53c279acc90 --count 1 --instance-type t2.micro --key-name timofey-1 --security-group-ids sg-id --subnet-id subnet-id --user-data file://./cloud-init.yaml --tag-specifications 'ResourceType=instance, Tags=[{Key=Name, Value=webServer}]'
```

Bei der Automatisierung der KN04 aufgabe, braucht es immer die gewissen Id's von erstellten objekten und mit richtingen berechtigungen einstellen. Man soll alle diese Befehle in eine shell skript aufschreiben und jede output vom Befehl einlesen und das nächste ausführen. Das dauert sehr lange, da man viele Befehle auswendig wissen muss und wie man sie alle verknüpft, damit alles funktioniert. Mit Terraform soll es vereinfachen, dass alles zu schreiben.

## B) Terraform

[Terraform File](./init.tf)

```bash
terraform init
terraform fmt
terraform apply
```

Bei Terraform muss ich nicht in eine bash skript schreiben, da Terraform das alles für mich

## C) Beliebige Erweiterungen